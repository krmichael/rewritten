import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';

import Home from './pages/Home';
import Forgot from './pages/Forgot';
import Signup from './pages/Signup';

import DashboardRoutes from './routes/dashboard.routes';

const Stack = createStackNavigator();

export default function Routes() {
  return (
    <Stack.Navigator
      initialRouteName="Home"
      screenOptions={{
        headerStyle: { backgroundColor: '#00804c' },
        headerTintColor: '#FFF',
      }}>
      <Stack.Screen
        name="Home"
        component={Home}
        options={{
          headerTransparent: true,
          headerTitle: null,
        }}
      />

      <Stack.Screen
        name="Dashboard"
        component={DashboardRoutes}
        options={{
          headerTitle: 'Welcome',
          headerTitleAlign: 'center',
        }}
      />

      <Stack.Screen
        name="forgot"
        component={Forgot}
        options={{
          headerTitle: 'Esqueceu a senha?',
        }}
      />

      <Stack.Screen
        name="signup"
        component={Signup}
        options={{
          headerTitle: 'Crie sua conta',
          headerTransparent: true,
          headerTintColor: '#00804c',
          headerTitleAlign: 'center',
        }}
      />
    </Stack.Navigator>
  );
}
