import React, { useState, useEffect } from 'react';
import { Keyboard, Animated } from 'react-native';

import {
  KeyboardAvoidingView,
  Container,
  Logo,
  Input,
  ButtonLogin,
  Button,
  ButtonTextForgot,
  ButtonTextSignUp,
} from './styles';

import logo from '../../assets/logo.png';

export default function Home({ navigation }) {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');

  const size = new Animated.Value(150);

  function navigateToSettings() {
    navigation.navigate('Dashboard');
  }

  function navigateToForgot() {
    navigation.navigate('forgot');
  }

  function navigateToSignup() {
    navigation.navigate('signup');
  }

  useEffect(() => {
    const didShow = Keyboard.addListener('keyboardDidShow', () => {
      Animated.timing(size, {
        toValue: 100,
        duration: 500,
      }).start();
    });

    const didHide = Keyboard.addListener('keyboardDidHide', () => {
      Animated.timing(size, {
        toValue: 150,
        duration: 500,
      }).start();
    });

    return () => {
      didShow.remove();
      didHide.remove();
    };
  }, [size]);

  return (
    <KeyboardAvoidingView>
      <Container>
        <Logo source={logo} style={{ width: size, height: size }} />

        <Input
          placeholder="Email"
          autoCapitalize="none"
          autoCorrect={false}
          keyboardType="email-address"
          value={email}
          onChangeText={(value) => setEmail(value)}
        />

        <Input
          placeholder="Senha"
          autoCapitalize="none"
          autoCorrect={false}
          secureTextEntry={true}
          value={password}
          onChangeText={(value) => setPassword(value)}
        />

        <ButtonLogin title="Entrar" onPress={navigateToSettings} />

        <Button onPress={navigateToForgot}>
          <ButtonTextForgot>Esqueceu a senha</ButtonTextForgot>
        </Button>

        <Button onPress={navigateToSignup}>
          <ButtonTextSignUp>Cadastre-se</ButtonTextSignUp>
        </Button>
      </Container>
    </KeyboardAvoidingView>
  );
}
