import styled from 'styled-components/native';
import { Platform, Animated } from 'react-native';

import RippleButton from '../../components/Button';

export const KeyboardAvoidingView = styled.KeyboardAvoidingView.attrs({
  behavior: Platform.OS === 'ios' && 'padding',
})`
  flex: 1;
  background-color: #f2f2f2;
`;

export const Container = styled.View`
  flex: 1;
  justify-content: center;
  align-items: center;
  margin-left: 20px;
  margin-right: 20px;
`;

export const Logo = styled(Animated.Image)`
  margin-top: 50px;
`;

export const Input = styled.TextInput`
  border-bottom-width: 1px;
  border-bottom-color: #bbb;
  width: 100%;
  margin-bottom: 10px;
`;

export const ButtonLogin = styled(RippleButton)`
  margin-top: 10px;
`;

export const Button = styled.TouchableOpacity.attrs({
  activeOpacity: 0.4,
})`
  padding: 7px;
  width: 100%;
  border-radius: 3px;
  align-items: center;
`;

export const ButtonTextForgot = styled.Text`
  color: #00804c;
  font-size: 12px;
  font-weight: 700;
  margin-top: 7px;
  text-transform: uppercase;
`;

export const ButtonTextSignUp = styled(ButtonTextForgot)`
  color: tomato;
  margin-top: 0;
`;
