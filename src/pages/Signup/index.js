import React from 'react';
import { Text, StatusBar } from 'react-native';

import { Container } from './styles';

export default function Signup() {
  return (
    <Container>
      <StatusBar
        backgroundColor="transparent"
        barStyle="dark-content"
        translucent={true}
      />
      <Text>Page Signup</Text>
    </Container>
  );
}
