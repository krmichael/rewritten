import React from 'react';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';

import Programation from '../pages/Dashboard/Programation';
import Participants from '../pages/Dashboard/Participants';

const Tab = createBottomTabNavigator();

export default function DashboardRoutes() {
  return (
    <Tab.Navigator
      tabBarOptions={{
        tabStyle: {
          paddingVertical: 10,
        },
      }}>
      <Tab.Screen
        name="Programation"
        component={Programation}
        options={{
          tabBarLabel: 'Programação',
        }}
      />
      <Tab.Screen
        name="Participants"
        component={Participants}
        options={{
          tabBarLabel: 'Participantes',
        }}
      />
    </Tab.Navigator>
  );
}
