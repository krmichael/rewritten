import React from 'react';
import { Animated } from 'react-native';

import { Button, ButtonView, ButtonText } from './styles';

export default function ButtonRipple({ title, background, ...props }) {
  const elevationAnim = new Animated.Value(2);

  function _onPressIn() {
    Animated.timing(elevationAnim, {
      toValue: 4,
      duration: 300,
      useNativeDriver: true,
    }).start();
  }

  function _onPressOut() {
    Animated.timing(elevationAnim, {
      toValue: 2,
      duration: 300,
      useNativeDriver: true,
    }).start();
  }

  return (
    <Button {...props} onPressIn={_onPressIn} onPressOut={_onPressOut}>
      <ButtonView
        {...props}
        background={background}
        style={{ elevation: elevationAnim }}>
        <ButtonText>{title}</ButtonText>
      </ButtonView>
    </Button>
  );
}
