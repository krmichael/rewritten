import styled from 'styled-components/native';
import { TouchableNativeFeedback, Platform, Animated } from 'react-native';

export const Button = styled.TouchableNativeFeedback.attrs({
  background:
    Platform.Version >= 21
      ? TouchableNativeFeedback.Ripple('#fff')
      : TouchableNativeFeedback.SelectableBackground(),
  useForeground: TouchableNativeFeedback.canUseNativeForeground(),
})``;

export const ButtonView = styled(Animated.View)`
  width: 100%;
  height: 50px;
  background-color: ${(props) => props.background || '#00804c'};
  justify-content: center;
  align-items: center;
  border-radius: 3px;
`;

export const ButtonText = styled.Text`
  font-size: 12px;
  font-weight: 700;
  text-transform: uppercase;
  color: #fff;
`;
