import React from 'react';
import { StatusBar } from 'react-native';
import { NavigationNativeContainer } from '@react-navigation/native';

import Routes from './routes';

export default function App() {
  return (
    <NavigationNativeContainer>
      <StatusBar
        backgroundColor="#00804c"
        barStyle="light-content"
        translucent={true}
      />

      <Routes />
    </NavigationNativeContainer>
  );
}
